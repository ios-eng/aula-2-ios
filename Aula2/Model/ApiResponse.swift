//
//  ApiResponse.swift
//  Aula2
//
//  Created by Charles Franca on 13/07/19.
//  Copyright © 2019 Charles Franca. All rights reserved.
//

import Foundation

struct ApiResponse: Codable {
    var results: Array<MovieModel>?
    var next: String?
    var previous: String?
    var count: Int?
}
