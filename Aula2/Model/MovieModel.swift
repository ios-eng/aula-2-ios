//
//  MovieModel.swift
//  Aula2
//
//  Created by Charles Franca on 13/07/19.
//  Copyright © 2019 Charles Franca. All rights reserved.
//

import Foundation

struct MovieModel : Codable {
    var name: String?
    var image: String?
    var overview: String?
    var url: String?
}
