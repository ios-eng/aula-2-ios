//
//  MovieTableViewCell.swift
//  Aula2
//
//  Created by Charles Franca on 13/07/19.
//  Copyright © 2019 Charles Franca. All rights reserved.
//

import UIKit

class MovieTableViewCell: UITableViewCell {
    @IBOutlet weak var imgMovie: UIImageView!
    @IBOutlet weak var lblMovieName: UILabel!
    @IBOutlet weak var txtOverview: UITextView!
    @IBOutlet weak var btView: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
