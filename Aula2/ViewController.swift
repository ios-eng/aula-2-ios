//
//  ViewController.swift
//  Aula2
//
//  Created by Charles Franca on 13/07/19.
//  Copyright © 2019 Charles Franca. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController {
    @IBOutlet weak var actMain: UIActivityIndicatorView!
    @IBOutlet weak var tableMovies: UITableView!
    
    var movies: Array<MovieModel> = []
    override func viewDidLoad() {
        super.viewDidLoad()
//        tableMovies.tableFooterView = UIView()
        // Do any additional setup after loading the view.
        showActivityIndicator()
        
        AF.request("http://localhost:3000/movies").responseDecodable { (response: DataResponse<ApiResponse>) in
            self.movies = (response.result.value?.results)!
            self.tableMovies.reloadData()
            self.hideActivityIndicator()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let nav = self.navigationController?.navigationBar
        
        // 2
        nav?.barStyle = UIBarStyle.black
        nav?.tintColor = UIColor.red
        nav?.topItem?.title = "Teste"
        nav?.backItem?.title = "Voltar"
    }
    
    func showActivityIndicator() {
        self.actMain.isHidden = false
        self.actMain.startAnimating()
    }
    
    func hideActivityIndicator() {
        self.actMain.isHidden = true
        self.actMain.stopAnimating()
    }
}


extension ViewController : UITableViewDelegate, UITableViewDataSource {
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "movieCell") as! MovieTableViewCell
        
        let movie: MovieModel = movies[indexPath.row]
//        var imgSource = movie.url?.replacingOccurrences(of: "https://pokeapi.co/api/v2/pokemon/", with: "")
//        imgSource = imgSource?.replacingOccurrences(of: "/", with: "")
//        imgSource = "https://pokeres.bastionbot.org/images/pokemon/\(imgSource ?? "").png"
//        movie.image = imgSource!
        
        cell.lblMovieName.text = movie.name
        cell.imgMovie.dowloadFromServer(link: movie.image!)
        cell.txtOverview.text = movie.overview
        cell.btView.tag = indexPath.row
        cell.btView.target(forAction: Selector(("buttonAction")), withSender: self)

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        print("Item Selected \(indexPath.row)")
    }
    
    @IBAction func buttonAction(_ sender: UIButton) {
        print("tapped")
    }
    
}
